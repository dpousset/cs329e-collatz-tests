#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
    def test_read_2(self):
        s = "500 505\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  500)
        self.assertEqual(j, 505)
    def test_read_3(self):
        s = "999 9999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  999)
        self.assertEqual(j, 9999)
    def test_read_4(self):
        s = "10 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 1)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # custom expected successes
    
    def test_eval_5(self):
        v = collatz_eval(1, 8000)
        self.assertEqual(v, 262)
    def test_eval_6(self):
        v = collatz_eval(999, 1999)
        self.assertEqual(v, 182)
    def test_eval_7(self):
        v = collatz_eval(999, 1001)
        self.assertEqual(v, 143)
    def test_eval_8(self):
        v = collatz_eval(1, 1000000)
        self.assertEqual(v, 525)
    def test_eval_9(self):
        v = collatz_eval(800, 600)
        self.assertEqual(v, 171)
    def test_eval_10(self):
        v = collatz_eval(22000, 22001)
        self.assertEqual(v, 44)
    def test_eval_11(self):
        v = collatz_eval(1, 2)
        self.assertEqual(v, 2)
    def test_eval_12(self):
        v = collatz_eval(999, 1999)
        self.assertEqual(v, 182)
    def test_eval_13(self):
        v = collatz_eval(35500, 35505)
        self.assertEqual(v, 218)
    def test_eval_14(self):
        v = collatz_eval(1000, 1001)
        self.assertEqual(v, 143)
    def test_eval_15(self):
        v = collatz_eval(2000, 8500)
        self.assertEqual(v, 262)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 1, 1000000, 525)
        self.assertEqual(w.getvalue(), "1 1000000 525\n")
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 1000, 1001, 143)
        self.assertEqual(w.getvalue(), "1000 1001 143\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    def test_solve_2(self):
        r = StringIO("999 1999\n1 2\n1 8000\n500 505\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "999 1999 182\n1 2 2\n1 8000 262\n500 505 111\n")
    def test_solve_3(self):
        r = StringIO("3 3\n199 201\n5 10\n15 30\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "3 3 8\n199 201 120\n5 10 20\n15 30 112\n")
    def test_solve_4(self):
        r = StringIO("1111 2222\n2222 4444\n4444 8888\n8888 16666\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1111 2222 182\n2222 4444 238\n4444 8888 262\n8888 16666 276\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage-3.5 run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage-3.5 report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
